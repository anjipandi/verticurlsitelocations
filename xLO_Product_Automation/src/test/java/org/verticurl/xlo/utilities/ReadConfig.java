package org.verticurl.xlo.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class ReadConfig {

	String configPath = "";
	Properties prop = null;
	Properties prop1 = null;
	private String trackName;
	File file = new File("./Configurations/inputConfig.properties");
	File file1 = new File("./Configurations/outputConfig.properties");

	// private final Logger log = Logger.getLogger(GenericAdapter.class);
	final Logger log = Logger.getLogger("EXCEPTION");

	public ReadConfig() {

		try {
			FileInputStream fis = new FileInputStream(file);

			prop = new Properties();

			prop.load(fis);

		} catch (Exception e) {
			System.out.println("Exception is" + e.getMessage());

		}
	}

	public Properties getConfigProperties() throws Exception {
		String configPath = "";
		trackName = prop.getProperty("trackname");
		String jobName = null;
		String workSpace = null;

		if (trackName != null)
			this.trackName = trackName;
		else {
			jobName = System.getenv("JOB_NAME");
			if (jobName != null) {
				String[] jobname = jobName.split("_");
				this.trackName = jobname[0];
			}
		}
		try {
			jobName = System.getenv("JOB_NAME");
			workSpace = System.getenv("WORKSPACE");

			configPath = "./Configurations/inputConfig.properties";

		} catch (Exception e) {
			configPath = System.getProperty("user.dir") + "/Configurations/inputConfig.properties";
			log.debug("Local configPath in catch" + configPath);
		}
		return prop;
	}

	public String getAppURL() {
		String url = prop.getProperty("baseURL");

		return url;

	}

	public String getUserName() {
		String user = prop.getProperty("userName");

		return user;
	}

	public String getPassword() {
		String pass = prop.getProperty("password");

		return pass;
	}

	public String getChromePath() {
		String chpath = prop.getProperty("chromedriverpath");

		return chpath;
	}

	public String getFirefoxPath() {
		String ffpath = prop.getProperty("ffdriverpath");

		return ffpath;
	}

	public String getIEPath() {
		String iepath = prop.getProperty("iedriverpath");

		return iepath;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getSSPath() {
		String sspath = prop.getProperty("sspath");

		return sspath;
	}

	public String getReportsPath() {
		String reportspath = prop.getProperty("reports");

		return reportspath;
	}

	public String getValueFromResource(String val) {
		String selectVal = prop.getProperty(val);
		return selectVal;
	}

	public void setValueToResource(String key, String value) throws IOException, ConfigurationException {
		/*prop1 = new Properties();
		String selectVal = (String) prop1.setProperty(key, value);
		FileOutputStream fos = new FileOutputStream(file1);
		prop1.store(fos, null);
		fos.close();
			return selectVal;
		*/
		PropertiesConfiguration conf = new PropertiesConfiguration(file1);
		conf.setProperty(key, value);
		conf.save();  
		
	
	}
	
	public String getValueFromResourceOutPut(String val) throws IOException {
		
		FileInputStream fis = new FileInputStream(file1);
		prop = new Properties();
		prop.load(fis);
		
		String selectVal = prop.getProperty(val);
		return selectVal;
	}

}
