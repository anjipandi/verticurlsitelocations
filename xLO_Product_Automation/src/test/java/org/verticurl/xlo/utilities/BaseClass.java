package org.verticurl.xlo.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.verticurl.xlo.Reporting.GenerateReports;
import org.verticurl.xlo.suitDrivers.DriverFactory;
import org.verticurl.xlo.suitDrivers.DriverType;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class BaseClass {

	ReadConfig config = new ReadConfig();

	public String baseURL = config.getAppURL();
	public String userName = config.getUserName();
	public String password = config.getPassword();
	public String sspath = config.getSSPath();

	public WebDriver driver;
	public static ThreadLocal<WebDriver> tdriver = new ThreadLocal<WebDriver>();

	// public static Logger logger = Logger.getLogger(BaseClass.class);
	public static Logger logger = Logger.getLogger(BaseClass.class);

	@BeforeTest
	@Parameters("browser")
	public void setUPTest(String browser) {
		PropertyConfigurator.configure("log4j.properties");

		BaseClass bs = new BaseClass();
		bs.init_Driver(browser);
		driver = getDriver();
		driver.get(baseURL);
	}

	public WebDriver init_Driver(String browser) {
		if (browser.equalsIgnoreCase("chrome")) {
			driver = DriverFactory.getDriver(DriverType.CHROME);
		}

		if (browser.equalsIgnoreCase("firefox")) {
			driver = DriverFactory.getDriver(DriverType.FIREFOX);
		}

		if (browser.equalsIgnoreCase("ie")) {
			driver = DriverFactory.getDriver(DriverType.IE);
		}
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		tdriver.set(driver);
		return getDriver();
	}

	public static synchronized WebDriver getDriver() {
		return tdriver.get();

	}

	@AfterTest
	public void tearDown() {
		driver.manage().deleteAllCookies();
		driver.quit();
	}

	public String captureScreen() throws IOException {
		String ssPath = null;
		System.out.println("path" + sspath);
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			ssPath = sspath + System.currentTimeMillis() + ".png";
			File target = new File(ssPath);

			FileUtils.copyFile(source, target);
			System.out.println("Screen shot taken at" + target);
		} catch (Exception e) {
			System.out.println("Find Screen" + e.getMessage());
		}

		return ssPath;

	}

	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
		// Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
		// Call getScreenshotAs method to create image file
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		// Move image file to new destination
		File DestFile = new File(fileWithPath);
		// Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);

	}

	@AfterSuite
	public void onFinish() {
		try {
			// source & destination directories
			File src = new File("./Allure-Files\\");
			File dest = new File("./allure-results\\");

			System.out.println("Source" + src);
			System.out.println("Destination" + dest);

			// copy all files and folders from `src` to `dest`
			FileUtils.copyDirectory(src, dest);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String newdir = System.getProperty("user.dir");
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec("cmd.exe /c cd \"" + newdir + "\" & start cmd.exe /k \"allure generate --clean\"");
			System.out.println("test");
			TimeUnit.SECONDS.sleep(5);

			rt.getRuntime().exec("taskkill /f /im cmd.exe");
			TimeUnit.SECONDS.sleep(2);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			File jsonFile = new File("./allure-report\\widgets\\summary.json");
			// Commons-IO
			String jsonString = FileUtils.readFileToString(jsonFile);

			JsonElement jelement = new JsonParser().parse(jsonString);
			JsonObject jobject = jelement.getAsJsonObject();
			System.out.println("filePath" + jobject);

			jobject.addProperty("reportName", "VERTICURL_AUTOMATION_REPORT - " + new SimpleDateFormat("ddMMyyyy").format(new Date()) + "");

			Gson gson = new Gson();

			String resultingJson = gson.toJson(jelement);
			// Commons-IO
			FileUtils.writeStringToFile(jsonFile, resultingJson);

			// Guava
			Files.write(resultingJson, jsonFile, Charsets.UTF_8);

			System.out.println("filePath" + jobject);
		} catch (Exception e) {

		}
	}

}
