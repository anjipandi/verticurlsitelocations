package org.verticurl.xlo.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import org.verticurl.suit.init.SuiteInit;
import org.verticurl.xlo.suitDrivers.BrowserDriver;

public class TestListener extends TestListenerAdapter {
	public int resultCounter=1;
	public static Map<Integer, Object[]> testresultdata = new LinkedHashMap<Integer, Object[]>();

	public void  ResultCollatorExcel(String browser,String testCaseClass,String testCase,String status){

		testresultdata.put(resultCounter, new Object[] {browser,testCaseClass, testCase, status});

	}




	public void onTestFailure(ITestResult result) {
		try {
			String browser = result.getTestContext().getCurrentXmlTest()
					.getParameter("browser");
			String rs = getMethodContext(result);
			ResultCollatorExcel(browser,result.getTestClass().getName(),result.getName(),"FAILED");
			System.err.println(rs + " | FAILED");
			//Some browser go extremely slow after taking screenshots)
			if (Arrays.asList(SuiteInit.Screenshot_Supported_Browsers).contains(browser)){
				takeScreenshot(result, browser);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}


	}


	@Override
	public void onTestStart(ITestResult result) {
		/*String browser = result.getTestContext().getCurrentXmlTest()
				.getParameter("browser");
		//EventFiringWebDriver driver = BrowserDriver.getDriver(browser);
		int val = 0;
		EventFiringWebDriver driver = null;
		Class cls = result.getTestClass().getRealClass().getSuperclass();
		
		try {
			Field fld = cls.getField("driverid");
			val = (int) fld.get(result.getInstance());
			System.out.println(val);
			
			Field fld2 = cls.getField("driver");
			driver = (EventFiringWebDriver) fld2.get(result.getInstance());
			
		} catch (NoSuchFieldException | SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.manage().deleteAllCookies();
		driver.navigate().refresh();
		// List<WebElement> List1 = driver.findElements(By.xpath(".//a[.='Logout']"));
		//if(List1.size()==0){
		
		}//}
*/
	}


	@Override
	public void onTestSkipped(ITestResult result) {
		String browser = result.getTestContext().getCurrentXmlTest()
				.getParameter("browser");
		String rs = getMethodContext(result);
		ResultCollatorExcel(browser,result.getTestClass().getName(),result.getName(),"SKIPPED");
		System.out.println(rs + " | SKIPPED");
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		String browser = result.getTestContext().getCurrentXmlTest()
				.getParameter("browser");
		String rs = getMethodContext(result);
		ResultCollatorExcel(browser,result.getTestClass().getName(),result.getName(),"PASSED");
		System.out.println(rs + " | PASSED");
	}

	public String getMethodContext(ITestResult result) {
		String browser = result.getTestContext().getCurrentXmlTest()
				.getParameter("browser");
		String testClasss = result.getTestClass().getName();
		String name = result.getName();
		String rs = browser + " | " + testClasss + " | " + name;
		if (resultCounter==1){
			ResultCollatorExcel("Browser","Test Class","Test Case","Status");
		}
		resultCounter=resultCounter+1;
		return rs;
	}

	/**
	 * This method takes the screenshot of the open webpage
	 * 
	 * @param result
	 * @param browser
	 */
	private void takeScreenshot(ITestResult result, String browser) {
		String currentDirPath = SuiteInit.dirPath;
		String fileName = browser + "_" + result.getName() + ".png";
		File file = new File(currentDirPath + "//" + fileName);
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		EventFiringWebDriver driver = BrowserDriver.getDriver(browser);
		//EventFiringWebDriver driver = (EventFiringWebDriver) result.getTestContext().getAttribute("driver");
		Object currentClass = result.getInstance();
		//EventFiringWebDriver driver = (EventFiringWebDriver) ((BaseClass) currentClass).getDriver();
		
		Augmenter augmenter = new Augmenter();
		TakesScreenshot ts = (TakesScreenshot) augmenter.augment(driver);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file);
			out.write(ts.getScreenshotAs(OutputType.BYTES));
			//String relativePath = SuiteInit.currentDir + "/" + fileName;
			//String absPath = file.getAbsolutePath();
			Reporter.log("<a href='..\\"+fileName+"' style='color:red;font-weight:bold'>Screenshot</a>");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	private void captureSource(ITestResult result, String browser) {
		String currentDirPath = SuiteInit.dirPath;
		String fileName = browser + "_" + result.getName() + ".txt";
		File file = new File(currentDirPath + "//" + fileName);
		EventFiringWebDriver driver = BrowserDriver.getDriver(browser);
		String source = driver.getPageSource();
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(file));
			out.write(source);
			String relativePath = SuiteInit.currentDir + "/" + fileName;
			Reporter.log("Source @: /" + relativePath);
			Reporter.log("<a href='/" + relativePath
					+ "'>Failed Page Source</a>");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
}