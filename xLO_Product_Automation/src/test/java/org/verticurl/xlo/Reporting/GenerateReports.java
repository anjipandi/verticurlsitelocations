package org.verticurl.xlo.Reporting;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.verticurl.xlo.utilities.BaseClass;

import io.qameta.allure.Attachment;

public class GenerateReports implements ITestListener {

	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();

	}

	@Attachment(value = "Web Page Screenshot", type = "image/png")
	public byte[] saveFailureScreenShot(WebDriver driver) {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}

	@Attachment(value = "{0}", type = "text/plain")
	public static String saveTextLog(String message) {
		return message;
	}

	@Override
	public void onTestStart(ITestResult iTestResult) {
		System.out.println("Execution of onTestStart Method" + getTestMethodName(iTestResult) + "  Started");

	}

	@Override
	public void onTestSuccess(ITestResult iTestResults) {
		System.out.println("*** Test execution " + getTestMethodName(iTestResults) + " passed...");
		System.out.println(iTestResults.getMethod().getMethodName() + " passed!");
/*
		Object testClass = iTestResults.getInstance();
		testClass.getClass();

		WebDriver driver = BaseClass.getDriver();
		if (driver instanceof WebDriver) {
			System.out.println("Sreenshot taken for test case " + getTestMethodName(iTestResults));
			saveFailureScreenShot(driver);
		}*/
		
		WebDriver driver = BaseClass.getDriver();
		saveFailureScreenShot(driver);
		
		saveTextLog(getTestMethodName(iTestResults) + "Passed and Screenshot taken for Pass");
	}

	@Override
	public void onTestFailure(ITestResult iTestResults) {
		System.out.println("*** Test execution " + getTestMethodName(iTestResults) + " failed...");
		System.out.println(iTestResults.getMethod().getMethodName() + " failed!");

		Object testClass = iTestResults.getInstance();
		testClass.getClass();

		WebDriver driver = BaseClass.getDriver();
		if (driver instanceof WebDriver) {
			System.out.println("Sreenshot taken for test case " + getTestMethodName(iTestResults));
			saveFailureScreenShot(driver);
		}
		saveTextLog(getTestMethodName(iTestResults) + "Failed and Screenshot taken for failure");
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("execution of onTestSkipped " + getTestMethodName(result) + "  Skipped");

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		System.out.println("Execution of onStart Method" + context.getName());
		context.setAttribute("WebDriver", BaseClass.getDriver());
	}

	@Override
	public void onFinish(ITestContext context) {
		System.out.println("Execution of onFinish Method" + context.getName());
		/*try {
			// source & destination directories
			File src = new File("./Allure-Files\\");
			File dest = new File("./allure-results\\");

			System.out.println("Source" + src);
			System.out.println("Destination" + dest);

			// copy all files and folders from `src` to `dest`
			FileUtils.copyDirectory(src, dest);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String newdir = System.getProperty("user.dir");
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec("cmd.exe /c cd \"" + newdir + "\" & start cmd.exe /k \"allure generate --clean\"");
			System.out.println("test");
			rt.getRuntime().exec("taskkill /f /im cmd.exe");
			TimeUnit.SECONDS.sleep(5);

		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			File jsonFile = new File("./allure-report\\widgets\\summary.json");
			// Commons-IO
			String jsonString = FileUtils.readFileToString(jsonFile);

			JsonElement jelement = new JsonParser().parse(jsonString);
			JsonObject jobject = jelement.getAsJsonObject();
			jobject.addProperty("reportName", "VERTICURL_AUTOMATION_REPORT");

			Gson gson = new Gson();

			String resultingJson = gson.toJson(jelement);
			// Commons-IO
			FileUtils.writeStringToFile(jsonFile, resultingJson);

			// Guava
			Files.write(resultingJson, jsonFile, Charsets.UTF_8);

			System.out.println("filePath" + jobject);
		} catch (Exception e) {

		}*/
	}

}
