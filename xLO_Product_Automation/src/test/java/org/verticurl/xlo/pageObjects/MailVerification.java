package org.verticurl.xlo.pageObjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.verticurl.xlo.utilities.GenericUtilities;

public class MailVerification extends GenericUtilities {

	WebDriver driver;

	public MailVerification(WebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "login")
	@CacheLookup
	WebElement searchUserMail;

	@FindBy(xpath = ".//span[contains(text(),'EXLO')]")
	@CacheLookup
	WebElement clickMail;

	@FindBy(xpath = ".//a[contains(text(),'Verify Email')]")
	@CacheLookup
	WebElement verifyEmailLink;

	@FindBy(xpath = "//h4[contains(text(),'Create New Password')]")
	@CacheLookup
	WebElement verifyNewWindow;

	@FindBy(id = "input-password")
	@CacheLookup
	WebElement inputPassword;

	@FindBy(id = "input-password-confirmation")
	@CacheLookup
	WebElement inputPasswordConf;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	@CacheLookup
	WebElement savePass;

	public void navigateToMailNotificationPage(WebDriver driver, String url) throws Exception {

		driver.get(url);
		pageLoadTime5Seconds(driver);

	}

	public void searchUserMail(WebDriver driver, String mailID) throws Exception {
		explicitWaitVisibility(driver, searchUserMail);
		if (searchUserMail.isDisplayed()) {
			searchUserMail.clear();
			searchUserMail.sendKeys(mailID);
			searchUserMail.sendKeys(Keys.ENTER);
			pageLoadTime5Seconds(driver);
		}

	}

	public void verifyClickMail(WebDriver driver) throws Exception {

		explicitWaitVisibility(driver, clickMail);
		if (clickMail.isDisplayed()) {
			click(clickMail);
			pageLoadTime5Seconds(driver);
		}
	}

	public void getVerifyEmailLink(WebDriver driver) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("ifmail")));
		explicitWaitVisibility(driver, verifyEmailLink);
		if (verifyEmailLink.isDisplayed()) {
			click(verifyEmailLink);
			pageLoadTime5Seconds(driver);
		}

	}

	boolean flag = false;

	public boolean verifyNewWindow(WebDriver driver) throws InterruptedException {
		TimeUnit.SECONDS.sleep(5);
		
		explicitWaitVisibility(driver, verifyNewWindow);
		if (verifyNewWindow.isDisplayed()) {
			flag = true;
		}

		return flag;
	}

	public void enterInputPassword(WebDriver driver, String password) throws Exception {

		explicitWaitVisibility(driver, inputPassword);
		if (inputPassword.isDisplayed()) {
			enterText(inputPassword, password);
			pageLoadTime5Seconds(driver);
		}

	}

	public void enterInputPasswordConf(WebDriver driver, String password) throws Exception {

		explicitWaitVisibility(driver, inputPasswordConf);
		if (inputPasswordConf.isDisplayed()) {
			enterText(inputPasswordConf, password);
			pageLoadTime5Seconds(driver);
		}

	}

	public void clickSavePass(WebDriver driver) throws Exception {
		explicitWaitVisibility(driver, savePass);
		if (savePass.isDisplayed()) {
			click(savePass);
			pageLoadTime5Seconds(driver);
		}
	}

}
