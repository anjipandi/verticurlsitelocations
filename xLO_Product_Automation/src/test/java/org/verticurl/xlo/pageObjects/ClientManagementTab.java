package org.verticurl.xlo.pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.verticurl.xlo.utilities.GenericUtilities;

public class ClientManagementTab extends GenericUtilities {
	WebDriver driver;

	public ClientManagementTab(WebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//a[@title='Clients']")
	@CacheLookup
	WebElement clientManagementTab;

	@FindBy(xpath = "//i[contains(text(),'group_add')]")
	@CacheLookup
	WebElement addUserButton;

	@FindBy(id = "role")
	@CacheLookup
	WebElement chooseUserType;

	@FindBy(id = "username")
	@CacheLookup
	WebElement userName;

	@FindBy(id = "firstname")
	@CacheLookup
	WebElement firstName;

	@FindBy(id = "lastname")
	@CacheLookup
	WebElement lastName;

	@FindBy(id = "client_display_name")
	@CacheLookup
	WebElement clientDisplayName;

	@FindBy(id = "client_name")
	@CacheLookup
	WebElement clientName;

	@FindBy(name = "client_description")
	@CacheLookup
	WebElement clientDescription;

	@FindBy(id = "client_email")
	@CacheLookup
	WebElement clientEmail;

	@FindBy(id = "client_contact")
	@CacheLookup
	WebElement clientMobile;

	@FindBy(id = "password_expiration")
	@CacheLookup
	WebElement choosePasswordExpiration;

	@FindBy(id = "limit_no_user")
	@CacheLookup
	WebElement selectUserLimit;

	@FindBy(id = "status")
	@CacheLookup
	WebElement selectStatus;

	@FindBy(id = "sso_enabled")
	@CacheLookup
	WebElement selectSsoEnabled;

	@FindBy(id = "client_management_form_submit")
	@CacheLookup
	WebElement clientSubmit;
	
	@FindBy(xpath = "//span[contains(text(),'Client successfully created.')]")
	@CacheLookup
	WebElement userCreateSuccMsg;

	@FindBy(id = "search_txt")
	@CacheLookup
	WebElement searchUser;

	public boolean navigateToClientManagementTab(WebDriver driver) throws Exception {
		boolean flag = false;
		logger.info("driver" + driver);
		explicitWaitVisibility(driver, clientManagementTab);
		if (clientManagementTab.isDisplayed()) {
			click(clientManagementTab);
			pageLoadTime5Seconds(driver);
			flag = true;

		}

		return flag;
	}

	public WebElement verifyAndClickAddUserButton(WebDriver driver) throws Exception {
		explicitWaitVisibility(driver, addUserButton);
		if (addUserButton.isDisplayed()) {
			click(addUserButton);
			pageLoadTime5Seconds(driver);

		}

		return addUserButton;
	}

	public void enterInputUserName(WebDriver driver, String usrName) throws Exception {
		explicitWaitVisibility(driver, userName);
		if (userName.isDisplayed()) {
			enterText(userName, usrName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void chooseUserType(WebDriver driver, String selectVal) {
		explicitWaitVisibility(driver, chooseUserType);
		if (chooseUserType.isDisplayed()) {
			enterText(chooseUserType, selectVal);
		}
	}

	public void enterFirstName(WebDriver driver, String fName) throws Exception {
		explicitWaitVisibility(driver, firstName);
		if (firstName.isDisplayed()) {
			enterText(firstName, fName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterLastName(WebDriver driver, String lName) throws Exception {
		explicitWaitVisibility(driver, lastName);
		if (lastName.isDisplayed()) {
			enterText(lastName, lName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterClientDisplayName(WebDriver driver, String clientName) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientDisplayName);

		explicitWaitVisibility(driver, clientDisplayName);
		if (clientDisplayName.isDisplayed()) {
			enterText(clientDisplayName, clientName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterClientName(WebDriver driver, String clntName) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientName);

		explicitWaitVisibility(driver, clientName);
		if (clientName.isDisplayed()) {
			enterText(clientName, clntName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterClientDesc(WebDriver driver, String clntDesc) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientDescription);

		explicitWaitVisibility(driver, clientDescription);
		if (clientDescription.isDisplayed()) {
			enterText(clientDescription, clntDesc);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterInputEmail(WebDriver driver, String email) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientEmail);

		explicitWaitVisibility(driver, clientEmail);
		if (clientEmail.isDisplayed()) {
			enterText(clientEmail, email);
			pageLoadTime5Seconds(driver);
		}

	}

	
	public void enterMobileNumber(WebDriver driver, String contactNum) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientMobile);

		explicitWaitVisibility(driver, clientMobile);
		if (clientMobile.isDisplayed()) {
			enterText(clientMobile, contactNum);
			pageLoadTime5Seconds(driver);
		}
	}

	public WebElement getChoosePasswordExpiration() {
		return choosePasswordExpiration;
	}
	
	public void selectPwdExpiry(WebDriver driver, String selectType,String selectVal) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", choosePasswordExpiration);

		explicitWaitVisibility(driver, choosePasswordExpiration);
		if (choosePasswordExpiration.isDisplayed()) {
			selectText(choosePasswordExpiration, selectType, selectVal);
			pageLoadTime5Seconds(driver);
		}
	}

	public WebElement getSelectUserLimit() {
		return selectUserLimit;
	}

	public WebElement getSelectStatus() {
		return selectStatus;
	}

	public WebElement getSelectSsoEnabled() {
		return selectSsoEnabled;
	}

	public WebElement getClientSubmit() {
		return clientSubmit;
	}
	
	public void clickSubmitBtn(WebDriver driver) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", clientSubmit);

		explicitWaitVisibility(driver, clientSubmit);
		if (clientSubmit.isDisplayed()) {
			click(clientSubmit);
			pageLoadTime5Seconds(driver);
		}
	}

	public WebElement getUserCreateSuccMsg() {
		return userCreateSuccMsg;
	}
	
	public String verifyUserCreateSuccMsg(WebDriver driver) throws Exception {
		explicitWaitVisibility(driver, userCreateSuccMsg);
		pageLoadTime30Seconds(driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", userCreateSuccMsg);
		explicitWaitVisibility(driver, userCreateSuccMsg);
		String successMsg=userCreateSuccMsg.getText();
		return successMsg;
	}

	public WebElement getSearchUser() {
		return searchUser;
	}

}
