package org.verticurl.xlo.pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.verticurl.xlo.utilities.GenericUtilities;

import io.qameta.allure.Step;

public class LoginPage extends GenericUtilities {

	WebDriver driver;

	public LoginPage(WebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//i[contains(text(),'email')]")
	@CacheLookup
	WebElement userNameIcon;

	@FindBy(xpath = ".//h4[@class='card-title']")
	@CacheLookup
	WebElement loginWindow;

	@FindBy(id = "username")
	@CacheLookup
	WebElement userName;

	@FindBy(id = "password")
	@CacheLookup
	WebElement password;

	@FindBy(xpath = "//button[@type='submit']")
	@CacheLookup
	WebElement loginSubmit;

	@FindBy(xpath = ".//div[@class='logo']")
	@CacheLookup
	WebElement validateAfterLogin;

	@FindBy(xpath = ".//small[contains(text(),'Logout')]")
	@CacheLookup
	WebElement logOutBtn;

	public boolean validateLoginWindow() throws Exception {
		boolean flag = false;
		explicitWaitVisibility(driver, loginWindow);
		if (loginWindow.isDisplayed()) {
			flag = true;
		}
		return flag;

	}

	public boolean checkUserNameIcon() throws Exception {
		boolean flag = false;
		explicitWaitVisibility(driver, userNameIcon);
		if (userNameIcon.isDisplayed()) {
			flag = true;
		}
		return flag;
	}

	@Step("Enter User Name {0}")
	public void checkUserNameAndEnter(String user) {
		explicitWaitVisibility(driver, userName);
		if (userName.isDisplayed()) {
			enterText(userName, user);
		} else {
			logger.info("User name is not available");
		}

	}

	@Step("Enter Password {0}")
	public void checkPasswordAndEnter(String pass) {
		explicitWaitVisibility(driver, password);
		if (password.isDisplayed()) {
			enterText(password, pass);
		} else {
			logger.info("password is not available");
		}

	}

	@Step("Click on Login Button")
	public void checkLoginButtonAndClick() throws Exception {
		explicitWaitVisibility(driver, loginSubmit);
		if (loginSubmit.isEnabled()) {
			click(loginSubmit);
		} else {
			logger.info("password is not available");
		}

	}
	@Step("Validate page after login")
	public boolean validatePageAfterLogin() {
		boolean flag = false;
		explicitWaitVisibility(driver, validateAfterLogin);

		if (validateAfterLogin.isDisplayed()) {
			flag = true;
		}

		return flag;

	}

	public boolean clickOnLogoutButton(WebDriver driver) {
		boolean flag = false;
		explicitWaitVisibility(driver, logOutBtn);
		if (logOutBtn.isDisplayed()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", logOutBtn);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", logOutBtn);
			flag = true;
		}

		return flag;

	}

}
