package org.verticurl.xlo.pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.verticurl.xlo.utilities.GenericUtilities;

public class UserManagementTab extends GenericUtilities {

	WebDriver driver;

	public UserManagementTab(WebDriver driver1) {
		driver = driver1;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//p[contains(text(),'User Management')]")
	@CacheLookup
	WebElement userManagementTab;

	@FindBy(xpath = ".//a[contains(.,' Add User')]")
	@CacheLookup
	WebElement addUserButton;

	@FindBy(id = "user_type")
	@CacheLookup
	WebElement chooseUserType;

	@FindBy(id = "input-username")
	@CacheLookup
	WebElement inputUserName;

	@FindBy(id = "input-email")
	@CacheLookup
	WebElement inputEmail;

	@FindBy(id = "input-firstname")
	@CacheLookup
	WebElement firstName;

	@FindBy(id = "input-lastname")
	@CacheLookup
	WebElement lastName;

	@FindBy(id = "input-mobile")
	@CacheLookup
	WebElement mobileNumber;

	@FindBy(id = "status")
	@CacheLookup
	WebElement selectStatus;

	@FindBy(xpath = "//button[contains(text(),'Add User')]")
	@CacheLookup
	WebElement createUserButton;

	@FindBy(xpath = "//span[contains(text(),'User successfully created.')]")
	@CacheLookup
	WebElement userCreateSuccMsg;

	@FindBy(id = "search_txt_user_list")
	@CacheLookup
	WebElement searchUser;

	public void chooseUserType(WebDriver driver, String selectType, String selectVal) {
		explicitWaitVisibility(driver, chooseUserType);
		if (chooseUserType.isDisplayed()) {
			selectText(chooseUserType, selectType, selectVal);
		}
	}

	public boolean navigateToUserManagementTab(WebDriver driver) throws Exception {
		boolean flag = false;
		logger.info("driver" + driver);
		explicitWaitVisibility(driver, userManagementTab);
		if (userManagementTab.isDisplayed()) {
			click(userManagementTab);
			pageLoadTime5Seconds(driver);
			flag = true;

		}

		return flag;
	}

	public WebElement verifyAndClickAddUserButton(WebDriver driver) throws Exception {
		explicitWaitVisibility(driver, addUserButton);
		if (addUserButton.isDisplayed()) {
			click(addUserButton);
			pageLoadTime5Seconds(driver);

		}

		return addUserButton;
	}

	public void enterInputUserName(WebDriver driver, String userName) throws Exception {
		explicitWaitVisibility(driver, inputUserName);
		if (inputUserName.isDisplayed()) {
			enterText(inputUserName, userName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterInputEmail(WebDriver driver, String email) throws Exception {
		explicitWaitVisibility(driver, inputEmail);
		if (inputEmail.isDisplayed()) {
			enterText(inputEmail, email);
			pageLoadTime5Seconds(driver);
		}

	}

	public void enterFirstName(WebDriver driver, String fName) throws Exception {
		explicitWaitVisibility(driver, firstName);
		if (firstName.isDisplayed()) {
			enterText(firstName, fName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterLastName(WebDriver driver, String lName) throws Exception {
		explicitWaitVisibility(driver, lastName);
		if (lastName.isDisplayed()) {
			enterText(lastName, lName);
			pageLoadTime5Seconds(driver);
		}
	}

	public void enterMobileNumber(WebDriver driver, String contactNum) throws Exception {
		explicitWaitVisibility(driver, mobileNumber);
		if (mobileNumber.isDisplayed()) {
			enterText(mobileNumber, contactNum);
			pageLoadTime5Seconds(driver);
		}
	}

	public WebElement getSelectStatus() {
		return selectStatus;
	}

	public void clickCreateUserButton(WebDriver driver) throws Exception {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", createUserButton);
		explicitWaitVisibility(driver, createUserButton);
		if (createUserButton.isDisplayed()) {
			click(createUserButton);
			pageLoadTime5Seconds(driver);
		}

	}

	public String verifyUserCreateSuccMsg(WebDriver driver) {
		explicitWaitVisibility(driver, userCreateSuccMsg);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", userCreateSuccMsg);
		String successMsg = userCreateSuccMsg.getText();
		return successMsg;
	}

	public WebElement getSearchUser() {
		return searchUser;
	}
}
