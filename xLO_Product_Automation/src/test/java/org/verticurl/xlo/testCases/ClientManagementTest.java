package org.verticurl.xlo.testCases;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.verticurl.xlo.Reporting.GenerateReports;
import org.verticurl.xlo.pageObjects.ClientManagementTab;
import org.verticurl.xlo.pageObjects.MailVerification;
import org.verticurl.xlo.utilities.BaseClass;
import org.verticurl.xlo.utilities.GenericUtilities;
import org.verticurl.xlo.utilities.ReadConfig;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ GenerateReports.class })
public class ClientManagementTest extends BaseClass {

	ClientManagementTab usertab = new ClientManagementTab(driver);
	ReadConfig config = new ReadConfig();
	GenericUtilities genUtility = new GenericUtilities();
	LoginTest login = new LoginTest();
	MailVerification mail = new MailVerification(driver);

	@Test
	@Description("Create Client Admin User")
	@Epic("EP002")
	@Feature("Feature1:CreateUser")
	@Story("Story:Client Admin User Creation")
	@Severity(SeverityLevel.BLOCKER)
	public void createClientAdminUser() throws Exception {
		logger.info("createClientAdminUser method is started");

		logger.info("Navigate to Client management Tab");

		try {
			boolean userTab = usertab.navigateToClientManagementTab(driver);
			if (userTab == true) {
				logger.info("clicking on add user Button");
				usertab.verifyAndClickAddUserButton(driver);

				// logger.info("Select User Type");
				// usertab.chooseUserType(driver, config.getValueFromResource("selectVal1"));

				String userName = config.getValueFromResource("inputUserName1")
						+ genUtility.generateRandomNumber4digi();
				logger.info("Enter User Name");
				usertab.enterInputUserName(driver, userName);
				config.setValueToResource("updatedUserName", userName);

				String firName = config.getValueFromResource("fiestName1");
				logger.info("Enter User First Name");
				usertab.enterFirstName(driver, firName);

				String lastName = config.getValueFromResource("lastName1");
				logger.info("Enter User Last Name");
				usertab.enterLastName(driver, lastName);

				String clientDisName = config.getValueFromResource("clientDisName") + genUtility.generateRandomChar();
				logger.info("Enter Client Display Name");
				usertab.enterClientDisplayName(driver, clientDisName);
				config.setValueToResource("updatedClientDispName", clientDisName);

				String clientName = config.getValueFromResource("clientName") + genUtility.generateRandomChar();
				logger.info("Enter Client Name");
				usertab.enterClientName(driver, clientName);

				String desc = config.getValueFromResource("desc");
				logger.info("Enter Description");
				usertab.enterClientDesc(driver, desc);

				String email = userName + "@yopmail.com";
				logger.info("Enter User email");
				usertab.enterInputEmail(driver, email);
				config.setValueToResource("updatedClientMail", email);

				String contNumber = config.getValueFromResource("contactNumber1");
				logger.info("Enter User Contact Number");
				usertab.enterMobileNumber(driver, contNumber);

				String pwdExp = config.getValueFromResource("pwdExpiry");
				logger.info("Select Expiry");
				usertab.selectPwdExpiry(driver, "selectByValue", pwdExp);

				logger.info("click on submit user");
				usertab.clickSubmitBtn(driver);

				logger.info("validating user creation success message");
				String succMessage = usertab.verifyUserCreateSuccMsg(driver);

				System.out.println("succMessage:-" + succMessage);
				System.out.println("succMessage1:-" + config.getValueFromResource("successMessage1"));

				Assert.assertEquals(succMessage, config.getValueFromResource("successMessage1"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Description("Activate Client Admin User")
	@Epic("EP002")
	@Feature("Feature1:ActivateUser")
	@Story("Story:Client Admin User Activate")
	@Severity(SeverityLevel.MINOR)
	public void verifyCreatedUserActivation() throws Exception {
		String mailerURL = config.getValueFromResource("mailerURL");
		String userMail = config.getValueFromResourceOutPut("updatedClientMail");
		String user = config.getValueFromResourceOutPut("updatedClientMail");
		String pwd = "Admin@123";
		logger.info("user" + user);
		logger.info("pwd" + pwd);

		logger.info("validating user creation success message");
		mail.navigateToMailNotificationPage(driver, mailerURL);

		logger.info("enter User Mail after creation");
		mail.searchUserMail(driver, userMail);

		// logger.info("click on User Mail");
		// mail.verifyClickMail(driver);

		logger.info("verify confirmation mail link and click on link");
		mail.getVerifyEmailLink(driver);

		int winSize = genUtility.switchWindows(driver);
		System.out.println("Size" + winSize);

		logger.info("verify rest pwd window");
		boolean windisp = mail.verifyNewWindow(driver);

		if (windisp == true) {

			logger.info("enter user password to set");
			mail.enterInputPassword(driver, "Admin@123");

			logger.info("enter confirmation pwd");
			mail.enterInputPasswordConf(driver, "Admin@123");

			logger.info("click on save");
			mail.clickSavePass(driver);
		}
		TimeUnit.SECONDS.sleep(2);
		login.userActivationLoginPage(user, pwd);
	}
}
