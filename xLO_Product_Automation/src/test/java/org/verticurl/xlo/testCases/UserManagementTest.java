package org.verticurl.xlo.testCases;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.verticurl.xlo.Reporting.GenerateReports;
import org.verticurl.xlo.pageObjects.MailVerification;
import org.verticurl.xlo.pageObjects.UserManagementTab;
import org.verticurl.xlo.utilities.BaseClass;
import org.verticurl.xlo.utilities.GenericUtilities;
import org.verticurl.xlo.utilities.ReadConfig;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ GenerateReports.class })
public class UserManagementTest extends BaseClass {

	UserManagementTab usertab = new UserManagementTab(driver);

	ReadConfig config = new ReadConfig();
	GenericUtilities genUtility = new GenericUtilities();
	LoginTest login = new LoginTest();
	MailVerification mail = new MailVerification(driver);

	@Test
	@Description("Create Super Admin User")
	@Epic("EP001")
	@Feature("Feature1:CreateUser")
	@Story("Story:Super Admin User Creation")
	@Severity(SeverityLevel.MINOR)
	public void createSuperAdminUser() throws Exception {

		logger.info("createSuperAdminUser method is started");

		try {
			logger.info("Navigate to user management Tab");
			boolean userTab = usertab.navigateToUserManagementTab(driver);

			if (userTab == true) {
				logger.info("clicking on add user Button");
				usertab.verifyAndClickAddUserButton(driver);

				logger.info("Select User Type");
				usertab.chooseUserType(driver, "selectByVisibleText", config.getValueFromResource("selectVal"));

				String userName = config.getValueFromResource("inputUserName") + genUtility.generateRandomNumber4digi();
				logger.info("Enter User Name");
				usertab.enterInputUserName(driver, userName);
				config.setValueToResource("updatedUserName", userName);

				String userMail = userName + "@yopmail.com";
				logger.info("Enter User Email");
				usertab.enterInputEmail(driver, userMail);
				config.setValueToResource("updatedUserMail", userMail);

				String firName = config.getValueFromResource("fiestName");
				logger.info("Enter User First Name");
				usertab.enterFirstName(driver, firName);

				String lastName = config.getValueFromResource("lastName");
				logger.info("Enter User Last Name");
				usertab.enterLastName(driver, lastName);

				String contNumber = config.getValueFromResource("contactNumber");
				logger.info("Enter User Contact Number");
				usertab.enterMobileNumber(driver, contNumber);

				logger.info("Click Create User button");
				usertab.clickCreateUserButton(driver);

				logger.info("validating user creation success message");
				String succMessage = usertab.verifyUserCreateSuccMsg(driver);
				System.out.println("message from UI" + succMessage);
				System.out.println("message from config" + config.getValueFromResource("successMessage"));
				Assert.assertEquals(succMessage, config.getValueFromResource("successMessage"));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Description("Activate Super Admin User")
	@Epic("EP001")
	@Feature("Feature1:ActivateUser")
	@Story("Story:Super Admin User Activate")
	@Severity(SeverityLevel.MINOR)
	public void verifyCreatedUserActivation() throws Exception {

		String mailerURL = config.getValueFromResource("mailerURL");
		String userMail = config.getValueFromResourceOutPut("updatedUserMail");
		String user = config.getValueFromResourceOutPut("updatedUserMail");
		String pwd = "Admin@123";
		logger.info("user" + user);
		logger.info("pwd" + pwd);

		logger.info("validating user creation success message");
		mail.navigateToMailNotificationPage(driver, mailerURL);

		logger.info("enter User Mail after creation");
		mail.searchUserMail(driver, userMail);

		// logger.info("click on User Mail");
		// mail.verifyClickMail(driver);

		logger.info("verify confirmation mail link and click on link");
		mail.getVerifyEmailLink(driver);

		int winSize = genUtility.switchWindows(driver);
		System.out.println("Size" + winSize);

		logger.info("verify rest pwd window");
		boolean windisp = mail.verifyNewWindow(driver);

		if (windisp == true) {

			logger.info("enter user password to set");
			mail.enterInputPassword(driver, "Admin@123");

			logger.info("enter confirmation pwd");
			mail.enterInputPasswordConf(driver, "Admin@123");

			logger.info("click on save");
			mail.clickSavePass(driver);
		}
		TimeUnit.SECONDS.sleep(2);
		login.userActivationLoginPage(user, pwd);
	}

}
