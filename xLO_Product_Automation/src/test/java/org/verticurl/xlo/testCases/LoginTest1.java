package org.verticurl.xlo.testCases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.AssertJUnit;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.verticurl.xlo.Reporting.GenerateReports;
import org.verticurl.xlo.pageObjects.LoginPage;
import org.verticurl.xlo.utilities.BaseClass;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ GenerateReports.class })
@Epic("EP002")
@Feature("Feature1:Login1")
@Story("Story:Login1")
public class LoginTest1 extends BaseClass {

	@Test(priority = 0, description="Invalid Login Scenario with wrong username and password")
	@Description("User is not able to login to the application Successfully")
	@Severity(SeverityLevel.BLOCKER)
	public void loginPage() throws IOException, InterruptedException {

		logger.info("Login Method is started");

		LoginPage login = new LoginPage(driver);
		TimeUnit.SECONDS.sleep(2);

		try {
			boolean logWind = login.validateLoginWindow();

			if (logWind) {
				logger.info("Entering User name");
				login.checkUserNameAndEnter(userName);

				logger.info("Entering User name");
				login.checkPasswordAndEnter(password);

				logger.info("Click on Login Button");
				login.checkLoginButtonAndClick();
				TimeUnit.SECONDS.sleep(40);

				boolean logVali = login.validatePageAfterLogin();

				AssertJUnit.assertEquals(true, false);
			}

		} catch (Exception e) {

			logger.info("Exception occured due to" + e.getMessage());
			e.printStackTrace();
		}

	}
}
