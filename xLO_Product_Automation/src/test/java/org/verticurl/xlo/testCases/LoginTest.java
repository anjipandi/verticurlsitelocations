package org.verticurl.xlo.testCases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.verticurl.xlo.Reporting.GenerateReports;
import org.verticurl.xlo.pageObjects.LoginPage;
import org.verticurl.xlo.utilities.BaseClass;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ GenerateReports.class })
@Epic("EP001")
@Feature("Feature1:Login")
@Story("Story:Login")
public class LoginTest extends BaseClass {

	@Test(priority = 0, description="Valid Login Scenario with valid username and password")
	@Description("User logged into to the application Successfully")
	@Severity(SeverityLevel.BLOCKER)
	public void loginPage() throws Exception {

		logger.info("Login Method is started");
		LoginPage login = new LoginPage(driver);

		try {
			boolean logWind = login.validateLoginWindow();
			if (logWind) {
				logger.info("Entering User name");
				login.checkUserNameAndEnter(userName);

				logger.info("Entering User name");
				login.checkPasswordAndEnter(password);

				logger.info("Click on Login Button");
				login.checkLoginButtonAndClick();
				TimeUnit.SECONDS.sleep(2);

				logger.info("Validate Page after login");
				boolean logVali = login.validatePageAfterLogin();

				logger.info("Validating Page after Login");
				Assert.assertEquals(logVali, true);

			}

		} catch (Exception e) {

			logger.info("Exception occured due to" + e.getMessage());
			e.printStackTrace();
		}

	}

	public void logOutPage() throws IOException, InterruptedException {

		LoginPage login = new LoginPage(driver);
		try {
			logger.info("click on Logout Button");
			boolean logout = login.clickOnLogoutButton(driver);

			if (logout == true) {
				logger.info("clicked on Logout Button and logout is successfull-");

			}
			driver.quit();

		} catch (Exception e) {
			driver.manage().deleteAllCookies();
			e.printStackTrace();
		}

	}

	@Description("user Activation LoginPage")
	@Epic("EP004")
	@Feature("Feature1:Login")
	@Story("Story:Login")
	@Severity(SeverityLevel.MINOR)
	public void userActivationLoginPage(String user, String pwd) throws IOException, InterruptedException {

		logger.info("Login Method is started");

		LoginPage login = new LoginPage(driver);
		TimeUnit.SECONDS.sleep(2);

		try {
			boolean logWind = login.validateLoginWindow();

			if (logWind) {
				logger.info("Entering User name");
				login.checkUserNameAndEnter(user);

				logger.info("Entering User name");
				login.checkPasswordAndEnter(pwd);

				logger.info("Click on Login Button");
				login.checkLoginButtonAndClick();

				TimeUnit.SECONDS.sleep(2);

				boolean logVali = login.validatePageAfterLogin();

				Assert.assertEquals(logVali, true);
			}

		} catch (Exception e) {

			logger.info("Exception occured due to" + e.getMessage());
			e.printStackTrace();
		}

	}
}
