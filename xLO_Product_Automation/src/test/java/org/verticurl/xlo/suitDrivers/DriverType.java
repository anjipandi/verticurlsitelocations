package org.verticurl.xlo.suitDrivers;

public enum DriverType {
	CHROME,
    FIREFOX,
    SAFARI,
    IE;
}
