package org.verticurl.xlo.suitDrivers;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.Architecture;

public class DriverFactory {
	private static final Map<DriverType, Supplier<WebDriver>> driverMap = new HashMap<>();

	// chrome driver supplier
	private static final Supplier<WebDriver> chromeDriverSupplier = () -> {
		//System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		return new ChromeDriver();
	};

	// firefox driver supplier
	private static final Supplier<WebDriver> firefoxDriverSupplier = () -> {
		//System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
		WebDriverManager.firefoxdriver().setup();
		return new FirefoxDriver();
	};

	// firefox driver supplier
	private static final Supplier<WebDriver> internetExplorerSupplier = () -> {
		//System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
		 WebDriverManager.iedriver().architecture(Architecture.X32).arch32().setup();
		//WebDriverManager.iedriver().setup();
		return new InternetExplorerDriver();
	};

	static {
		driverMap.put(DriverType.CHROME, chromeDriverSupplier);
		driverMap.put(DriverType.FIREFOX, firefoxDriverSupplier);
		driverMap.put(DriverType.IE, internetExplorerSupplier);
	}

	// return a new driver from the map
	public static final WebDriver getDriver(DriverType type) {
		return driverMap.get(type).get();
	}
}
