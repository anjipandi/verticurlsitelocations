package org.verticurl.xlo.suitDrivers;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.verticurl.xlo.utilities.ReadConfig;

public class DriverInit {
	private RemoteWebDriver fireFoxDriver;
	private RemoteWebDriver chromeDriver;
	private RemoteWebDriver IEDriver;
	
	public static DriverInit driverInstance = null;
	ReadConfig config = new ReadConfig();

	
	
	private DriverInit() {
		URL url = null;
		

		try {
			String s = config.getAppURL();
			url = new URL(s);

		} catch (MalformedURLException e) {
			System.err.println(e.getMessage());
		}

		try {
			FirefoxProfile profile = new FirefoxProfile();
			/*
			 * String waveExtension = Util.getValFromResource("wave.toolbar",
			 * FileType.CONFIG_FILE); profile.addExtension(new File(waveExtension));
			 */
			DesiredCapabilities dwf = DesiredCapabilities.firefox();
			dwf.setPlatform(Platform.WINDOWS);
			dwf.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			dwf.setCapability("unexpectedAlertBehaviour", "accept");
			dwf.setCapability(FirefoxDriver.PROFILE, profile);
			fireFoxDriver = new RemoteWebDriver(url, dwf);
		} catch (Exception e) {
			System.err.println("Exception in starting firefox driver: " + e.getMessage());
			e.printStackTrace();
		}

		try {
			DesiredCapabilities dwc = DesiredCapabilities.chrome();
			dwc.setPlatform(Platform.WINDOWS);
			dwc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			dwc.setCapability("unexpectedAlertBehaviour", "accept");
			chromeDriver = new RemoteWebDriver(url, dwc);
		} catch (Exception e) {
			System.err.println("Exception in starting chrome driver: " + e.getMessage());
		}
		try {

			DesiredCapabilities c = DesiredCapabilities.internetExplorer();
			c.setCapability("nativeEvents", false);
			c.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
			c.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			c.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
			c.setCapability("REQUIRE_WINDOW_FOCUS", false);
			c.setCapability("unexpectedAlertBehaviour", "accept");

			IEDriver = new RemoteWebDriver(url, c);
		} catch (Exception e) {
			System.err.println("Exception in starting IE driver: " + e.getMessage());
		}

	}

	public static DriverInit getDriverInstance() {
		if (driverInstance == null) {
			driverInstance = new DriverInit();
		}
		return driverInstance;
	}

	public RemoteWebDriver getFireFoxDriver() {
		return fireFoxDriver;
	}

	public RemoteWebDriver getChromeDriver() {
		return chromeDriver;
	}

	public RemoteWebDriver getIEDriver() {
		return IEDriver;
	}

}
