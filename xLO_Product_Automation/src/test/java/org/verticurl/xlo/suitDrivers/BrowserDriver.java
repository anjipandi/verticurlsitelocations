package org.verticurl.xlo.suitDrivers;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class BrowserDriver {
public static EventFiringWebDriver getDriver(String browser) {
		
		RemoteWebDriver Wdriver = null;
		EventFiringWebDriver driver =null;
		if (browser.equals("firefox")) {
			Wdriver = DriverInit.getDriverInstance().getFireFoxDriver();
			driver=eventRegister(Wdriver);
			driver.manage().window().maximize();
		}  else if (browser.equals("chrome")) {
			Wdriver = DriverInit.getDriverInstance().getChromeDriver();
			driver=eventRegister(Wdriver);
			driver.manage().window().maximize();
		} else if (browser.equals("iexplore")) {
			Wdriver = DriverInit.getDriverInstance().getIEDriver();
			driver=eventRegister(Wdriver);
			driver.manage().window().maximize();
		} 
		
			
		return driver;
	}
	
	public static EventFiringWebDriver eventRegister(RemoteWebDriver Wdriver){
		EventFiringWebDriver driver= new EventFiringWebDriver(Wdriver);
		//TestEventHandler handler = new TestEventHandler();
		//driver.register(handler);
		return driver;
	}

}
