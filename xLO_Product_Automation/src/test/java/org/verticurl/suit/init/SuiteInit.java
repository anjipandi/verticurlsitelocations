/**
 * 
 */
package org.verticurl.suit.init;

import java.util.Date;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.verticurl.xlo.listeners.TestListener;
import org.verticurl.xlo.suitDrivers.DriverInit;

public class SuiteInit {
	private DriverInit driverInstance;
	public static String currentDir;
	public static String dirPath;
	public static final String[] Screenshot_Supported_Browsers = { "firefox", "chrome", "iexplore", "safari",
			"MACchrome", "MACfirefox" };
	// define an Excel Work Book
	public HSSFWorkbook workbook;
	// define an Excel Work sheet
	public HSSFSheet sheet;

	@BeforeSuite
	public void init() {
		driverInstance = DriverInit.getDriverInstance();
	//	createDirectory();
	}

	@AfterSuite
	public void closeAll() {
		//storeFilePath(dirPath);
		try {
			driverInstance.getFireFoxDriver().quit();
		} catch (Exception e) {
			System.err.println("Exception in quiting the Firefox driver: " + e.getMessage());
		}

		try {
			driverInstance.getChromeDriver().quit();
		} catch (Exception e) {
			System.err.println("Exception in quiting the Chrome driver: " + e.getMessage());
		}
		try {
			driverInstance.getIEDriver().quit();
		} catch (Exception e) {
			System.err.println("Exception in quiting the IE driver: " + e.getMessage());
		}

	}

	/**
	 * This method writes test results onto excel
	 */

	public void writeResultsExcel() {
		// write excel file and file name is TestResult.xls

		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet("Test Result");
		TestListener tl = new TestListener();
		Set<Integer> keyset = tl.testresultdata.keySet();

		int rownum = 0;
		for (Integer key : keyset) {
			HSSFRow row = sheet.createRow(rownum++);
			Object[] objArr = tl.testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				HSSFCell cell = row.createCell(cellnum++);
				if (obj instanceof Date)
					cell.setCellValue((Date) obj);
				else if (obj instanceof Boolean)
					cell.setCellValue((Boolean) obj);
				else if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Double)
					cell.setCellValue((Double) obj);
			}
		}
	}
}
