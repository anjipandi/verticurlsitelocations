package org.verticurl.xlo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App {

	static String filePath = "./allure-report\\widgets\\summary.json";

	public static void main(String[] args) throws IOException {
		String fileSuffix = new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date());
		System.out.println("Date"+fileSuffix);
	}

}
